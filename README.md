#Hart Coding Challenge

### Challenge:
Build a Node application that datamines Yelp & Foursquare for all (Restuaruants) -- this is a category-- inside of a county in Texas or California (hint: You will have to deal with pagination and utilizing zip codes for search). Insert this data into a Mongo, Redis, Elastic Search or a nosql server, and then compare the data and create a new collection where the data matches in both Yelp & Foursquare to hygiene the data and insert this data into a nosql database(mongo, couch, etc). In addition to this, use the [what3words](http://what3words.com/) api to apply a three word location for the hygiened records.

*  Be prepared to explain your logic and how you determined which datapoints would be placed where.

### Bonus:
- Add unit testing
- Create an express or koa web application that allows the user to change the category 

# Hart Coding Challenge

  This application uses Koa and I have chosen Orange County for the challenge.

## Tasks:

- Scaffold App
⋅⋅* Use Yeoman to generate basic koa app
⋅⋅* Set up unit testing
⋅⋅* Set up integration testing
⋅⋅* Set up MongoDB
⋅⋅* Get zip codes for Orange County
- Pull data from Yelp
⋅⋅* Obtain API Key
⋅⋅* Create secret file to store keys
⋅⋅* Save keys to secret
⋅⋅* Logic for making API call
- Pull data from Foursquare
⋅⋅* Obtain API Key
⋅⋅* Save keys to secret
⋅⋅* Logic for making API call
- Match data from Yelp + Foursquare
⋅⋅* Logic for matching data
- Clean matched data
⋅⋅* Logic for data manipulation
- Save cleaned data into MongoDB
⋅⋅* Define schema
⋅⋅* Logic for saving data
- Take GET request to return JSON of cleaned data
⋅⋅* Logic for GET request for data
⋅⋅* Logic for querying database
- Take GET request to match location via what3words
⋅⋅* Obtain API Key
⋅⋅* Save keys to secret
⋅⋅* Logic for GET request to match data
⋅⋅* Logic for making API call





