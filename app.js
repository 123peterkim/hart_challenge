'use strict';
require('dotenv').config();
const messages = require('./controllers/messages');
const restaurants = require('./controllers/restaurants');

const compress = require('koa-compress');
const logger = require('koa-logger');
const serve = require('koa-static');
const route = require('koa-route');
const koa = require('koa');
const path = require('path');
const app = module.exports = koa();
const mongoose = require('koa-mongoose');

app.use(mongoose({
  mongoose: require('mongoose-q')(),
  username: '',
  password: '',
  host: 'localhost',
  port: 27017,
  database: 'hart-challenge',
  db: {
    native_parser: true
  },
  server: {
    poolSize: 5
  }
}));

// Logger
app.use(logger());

app.use(route.get('/', restaurants.fetch));
app.use(route.get('/messages', messages.list));
app.use(route.get('/messages/:id', messages.fetch));
app.use(route.post('/messages', messages.create));
app.use(route.get('/async', messages.delay));
app.use(route.get('/promise', messages.promise));

// Compress
app.use(compress());

if (!module.parent) {
  app.listen(3000);
  console.log('listening on port 3000');
}
