'use strict';

const oauthSignature = require('oauth-signature');
const n = require('nonce')();
const qs = require('querystring');
const _ = require('lodash');
const zipCodes = require('../seed/zipCodes');

module.exports.yelp = function(set_parameters) {
	/* The type of request */
	const httpMethod = 'GET';

	/* The url we are using for the request */
	const url = 'http://api.yelp.com/v2/search';

	/* We can setup default parameters here */
	const default_parameters = {
		location: zipCodes[0],
		term: 'restaurants'
	};

	/* We set the require parameters here */
	const required_parameters = {
		oauth_consumer_key : process.env.YELP_CONS_KEY,
		oauth_token : process.env.YELP_TOKEN,
		oauth_nonce : n(),
		oauth_timestamp : n().toString().substr(0,10),
		oauth_signature_method : 'HMAC-SHA1',
		oauth_version : '1.0'
	};

	/* We combine all the parameters in order of importance */
	const parameters = _.assign(default_parameters, set_parameters, required_parameters);

	/* We set our secrets here */
	const consumerSecret = process.env.YELP_CONS_SECRET;
	const tokenSecret = process.env.YELP_TOKEN_SECRET;

	/* Then we call Yelp's Oauth 1.0a server, and it returns a signature */
	/* Note: This signature is only good for 300 seconds after the oauth_timestamp */
	const signature = oauthSignature.generate(httpMethod, url, parameters, consumerSecret, tokenSecret, { encodeSignature: false});

	/* We add the signature to the list of paramters */
	parameters.oauth_signature = signature;

	/* Then we turn the paramters object, to a query string */
	const paramURL = qs.stringify(parameters);

	/* Add the query string to the url */
	const apiURL = url+'?'+paramURL;

	/* Then we use request to send make the API Request */
	return apiURL;
};

module.exports.fs = function(obj){
	let path = '/venues/search';
	if (obj) {
		const requestData = qs.stringify(obj);
		path += '?'+requestData+'&';
	} else {
		path += `?near=${zipCodes[0]}&categoryId=4d4b7105d754a06374d81259`
	};
	path = path+'&client_id='+process.env.FS_ID+'&client_secret='+process.env.FS_SECRET+'&v=20160711';
	const requestOptions = {
		host: "http://api.foursquare.com",
		method: 'GET',
		port: 443,
		path: "/v2"+path,
		headers: {
			'Accept': 'text/json',
			'Accept-Language': this.locale,
			'Content-Type': 'application/x-www-form-urlencoded',
			'Content-Length': '0'
		}
	};
	return requestOptions.host+requestOptions.path;
};