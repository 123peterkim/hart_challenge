'use strict';

const parse = require('co-body');
const JSONStream = require('JSONStream');
const request = require('request');
const config = require('./config');
const stream = require('stream');
const _ = require('lodash');
const mongoose = require('koa-mongoose');
const Restaurants = require('../schemas/restaurants');

module.exports.fetch = function *(next) {
	this.type = 'json';
	const stream = this.body = JSONStream.stringify();
	let newYelpData = [];
	let newFSData = [];

	request(config.yelp(), function (error, response, yelpData) {
		if (error) error => this.throw(error);

		yelpData = JSON.parse(yelpData);
		_.forEach(yelpData.businesses, function(obj) {
			newYelpData.push(_.pick(obj, ['name', 'phone']));
		});

		request(config.fs(), function (error, response, fsData) {
			if (error) error => this.throw(error);
			fsData = JSON.parse(fsData);
			_.forEach(fsData.response.venues, function(obj) {
				let newObj = obj;
				newObj = _.pick(obj, ['name']);
				newObj.phone = obj.contact.phone;
				newFSData.push(obj);
			});
			let dataEntry = {};
			dataEntry.restaurants = newYelpData;
			const entryToSave = new Restaurants(dataEntry);

			entryToSave.saveQ();
		})
	})
};



module.exports.fs = function *(next) {
	this.type = 'json';
	const stream = this.body = JSONStream.stringify();


};