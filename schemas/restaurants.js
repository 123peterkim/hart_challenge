const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const RestaurantsSchema = new Schema({
	restaurants: Array
});


module.exports = mongoose.model('Restaurants', RestaurantsSchema);