'use strict';
const restaurants = require('../../controllers/restaurants');
const config = require('../../controllers/config');
const co = require('co');
const assert = require('assert');

describe('Fetch', () => {
	describe('Yelp', () => {
		it('should return valid query string');
		it('should return JSON');
		it('should throw error');
	});

	describe('FS', () => {
		it('should return valid query string');
		it('should return JSON');
		it('should throw error');
	});
});

